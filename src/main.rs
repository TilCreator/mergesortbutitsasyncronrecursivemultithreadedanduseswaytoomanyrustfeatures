#![feature(is_sorted)]
#![feature(exact_size_is_empty)]

use failure::Error;
use rand::seq::SliceRandom;
use rand::thread_rng;
use std::env::args;

mod merge_sort {
    use async_recursion::async_recursion;

    pub async fn merge_sort<T>(vec: Vec<T>) -> Vec<T>
    where
        T: Ord + Send + 'static,
    {
        split(vec).await
    }

    #[async_recursion]
    async fn split<T>(mut vec: Vec<T>) -> Vec<T>
    where
        T: Ord + Send + 'static,
    {
        let vec_length = vec.len();

        if vec_length <= 1 {
            vec
        } else {
            let (vec, vec_other) = tokio::join!(
                tokio::spawn(split(vec.split_off(vec_length / 2))),
                tokio::spawn(split(vec))
            );

            merge(vec.unwrap(), vec_other.unwrap()).await
        }
    }

    async fn merge<T>(vec: Vec<T>, vec_other: Vec<T>) -> Vec<T>
    where
        T: Ord + Send + 'static,
    {
        let mut out = vec![];
        let mut iter = vec.into_iter().peekable();
        let mut iter_other = vec_other.into_iter().peekable();

        loop {
            if iter.is_empty() {
                out.append(&mut iter_other.collect());
                break;
            } else if iter_other.is_empty() {
                out.append(&mut iter.collect());
                break;
            } else if iter.peek().unwrap() < iter_other.peek().unwrap() {
                out.push(iter.next().unwrap());
            } else {
                out.push(iter_other.next().unwrap());
            }
        }

        out
    }
}

use merge_sort::merge_sort;

#[tokio::main]
async fn main() -> Result<(), Error> {
    let mut vec: Vec<usize> = (0..(args()
        .nth(1)
        .map(|arg| arg.parse::<usize>().expect("Arg1 should be an integer"))
        .unwrap_or(100)))
        .collect();
    vec.shuffle(&mut thread_rng());

    println!("Shuffled vector\nsorting...");

    let vec_sorted = merge_sort(vec).await;

    println!("sorted: {}", vec_sorted.iter().is_sorted());

    Ok(())
}
